﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MODELO;

namespace WEB.Controllers
{
    public class ProductosController : Controller
    {
        SISTEMA_TIENDAEntities st = new SISTEMA_TIENDAEntities();
        
        public ActionResult NuevoProducto() //Accion GET de NuevoProducto
        {
            ViewBag.Medida = new SelectList(st.T_MEDIDA.ToList(), "Id", "Descripcion");
            return View();
        }

        [HttpPost]
        public ActionResult NuevoProducto(T_NUEVO_PRODUCTO nuevo) //Accion 
        {
            try
            {
                st.SP_NUEVO_PRODUCTO(nuevo.Codigo, nuevo.Descripcion, nuevo.IdMedida, nuevo.Volumen);
                ViewBag.Mensaje = "ok";
            }
            catch (Exception)
            {
                ViewBag.Mensaje = "no";
            }
            ViewBag.Medida = new SelectList(st.T_MEDIDA.ToList(), "Id", "Descripcion");
            return View("NuevoProducto");
        }
    }
}