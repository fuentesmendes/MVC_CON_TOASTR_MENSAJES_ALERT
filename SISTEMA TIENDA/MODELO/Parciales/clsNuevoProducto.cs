﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UTILIDADES.MENSAJES;

namespace MODELO
{
    [MetadataType(typeof(clsNuevoProducto))] //Se agrega esta linea con el nombre de la clase creada
    public partial class T_NUEVO_PRODUCTO //Se instancia como parcial la clase del modelo a manipular
    {

    }
    class clsNuevoProducto
    {
        [Display(Name = "CODIGO")]
        [Required(ErrorMessageResourceName = "CAMPOREQUERIDO", ErrorMessageResourceType = typeof(MensajesTienda))]
        [MaxLength(20,ErrorMessageResourceName = "LIMITEEXCEDIDO", ErrorMessageResourceType = typeof(MensajesTienda))]
        public string Codigo { get; set; }
        [Display(Name = "NOMBRE")]
        [Required(ErrorMessageResourceName = "CAMPOREQUERIDO", ErrorMessageResourceType = typeof(MensajesTienda))]
        [MaxLength(50, ErrorMessageResourceName = "LIMITEEXCEDIDO", ErrorMessageResourceType = typeof(MensajesTienda))]
        public string Descripcion { get; set; }
        [Display(Name = "VOLUMEN")]
        [Required(ErrorMessageResourceName = "CAMPOREQUERIDO", ErrorMessageResourceType = typeof(MensajesTienda))]
        public double Volumen { get; set; }
        [Display(Name = "MEDIDA")]
        public int IdMedida { get; set; }
    }
}
